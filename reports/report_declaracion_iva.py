from openerp.report import report_sxw
from openerp.osv import osv

class report_nombre(report_sxw.rml_parse):
    def __init__(self,cr,uid,name,context):
        super(report_nombre,self).__init__(cr,uid,name,context)

class report_nombre_des(osv.AbstractModel):
    _name = "report.resumen_declaracion_iva.libro.resumen_iva"
    _inherit = "report.abstract_report"
    _template = "resumen_declaracion_iva.template_report_resumen_iva"
    _wrapped_report_class = resumen_iva

    
