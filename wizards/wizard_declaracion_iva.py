# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.


from datetime import datetime, timedelta
from odoo import models, api, fields
from odoo.exceptions import ValidationError
from odoo.tools.misc import formatLang
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT
import xlwt
import base64
import cStringIO
#prueba

from logging import getLogger ##Importa las librerias para los comandos de almacenamiento de registros para las incidencias

import urllib

_logger = getLogger(__name__) ##Permite guardar un registro de incidencias que se presenten

class resumen_iva(models.Model):
    _name = "account.wizard.resumen.iva" ## = nombre de la carpeta.nombre del archivo deparado con puntos

    facturas_ids = fields.Many2many('account.invoice', string='Facturas', store=True) ##Relacion con el modelo de la vista de la creacion de facturas
    retiva_ids = fields.Many2many('snc.retiva.partners.lines', string='Retiva', store=True)

    date_from = fields.Date('Date From') # creacion de campo de fecha de entrada
    date_to = fields.Date('Date To') # creacion de campo de fecha de salida

    # fields for download xls
    state = fields.Selection([('choose', 'choose'), ('get', 'get')],default='choose') ##Genera los botones de exportar xls y pdf como tambien el de cancelar
    report = fields.Binary('Prepared file', filters='.xls', readonly=True)
    name = fields.Char('File Name', size=32)
    company_id = fields.Many2one('res.company','Company',default=lambda self: self.env.user.company_id.id)





    def cal_general(self):
        # Searching for customer invoices
        self.invoices_v = self.env['account.invoice'].search([('date_invoice', '<=', self.date_to),('date_invoice','>=',self.date_from),('type','!=','in_invoice'),('type', '!=', 'in_refund'), ('state', '!=', 'draft'),('state', '!=', 'cancel')])

        self.invoices_c = self.env['account.invoice'].search([('date_invoice', '<=', self.date_to), ('date_invoice', '>=', self.date_from),('type', '!=', 'out_invoice'),('type', '!=', 'out_refund'), ('state', '!=', 'draft'),('state', '!=', 'cancel')])

        tot_execto_mes_v = 0

        tot_alicu_general_base_v = 0
        tot_alicu_general_debi_v = 0
        tot_alicu_general_reten_v = 0

        tot_alicu_reducida_base_v = 0
        tot_alicu_reducida_debi_v = 0
        tot_alicu_reducida_reten_v = 0

        tot_alicu_gener_adic_base_v = 0
        tot_alicu_gener_adic_debi_v = 0
        tot_alicu_gener_adic_reten_v = 0

       # Calculos de Ventas item 1 - 9 DÉBITOS FISCALES
        for invoice_v in self.invoices_v:

            ids_accounts = invoice_v.id
            for l in invoice_v.invoice_line_ids:
                if l.invoice_id.id == ids_accounts:
                    if l.invoice_line_tax_ids.amount or l.invoice_line_tax_ids.amount == 0:

                        if invoice_v.origin == 0:
                            if l.invoice_line_tax_ids.amount == 0:

                               # Ventas Internas no Gravadas item 1
                                tot_execto_mes_v += l.price_subtotal

                            elif l.invoice_line_tax_ids.amount != 0:

                               # Calculos de Alicuaotas
                                alicuota_v = ((l.price_subtotal*l.invoice_line_tax_ids.amount)/100)

                               # Calculos de Retencion
                                reten_iva_v = ((alicuota_v * invoice_v.retiva_id.porc_ret) / 100)

                               # Ventas Internas Gravadas por Alicuota General item 3
                                # Iva General
                                if l.invoice_line_tax_ids.amount == 16:
                                    tot_alicu_general_base_v += l.price_subtotal        # Base imponible
                                    tot_alicu_general_debi_v += alicuota_v              # DÉBITO FISCAL
                                    tot_alicu_general_reten_v += reten_iva_v

                               # Ventas Internas Gravadas por Alicuota General más Adicional item 4
                                # Iva general + adicional 16% + 15%
                                elif l.invoice_line_tax_ids.amount == 31:
                                    tot_alicu_gener_adic_base_v += l.price_subtotal     # Base imponible
                                    tot_alicu_gener_adic_debi_v += alicuota_v           # DÉBITO FISCAL
                                    tot_alicu_gener_adic_reten_v += reten_iva_v

                               # Ventas Internas Gravadas por Alicuota Reducida item 5
                                # Iva Reducido
                                elif l.invoice_line_tax_ids.amount == 8:
                                    tot_alicu_reducida_base_v += l.price_subtotal       # Base imponible
                                    tot_alicu_reducida_debi_v += alicuota_v             # DÉBITO FISCAL
                                    tot_alicu_reducida_reten_v += reten_iva_v

                        else:
                            if l.invoice_line_tax_ids.amount == 0:

                                tot_execto_mes_v -= l.price_subtotal

                            elif l.invoice_line_tax_ids.amount != 0:


                                # Calculos de Alicuaotas
                                alicuota_v = ((l.price_subtotal * l.invoice_line_tax_ids.amount) / 100)

                                # Calculos de Retencion
                                reten_iva_v = ((alicuota_v * invoice_v.retiva_id.porc_ret) / 100)

                                # Ventas Internas Gravadas por Alicuota General item 3
                                # Iva General
                                if l.invoice_line_tax_ids.amount == 16:
                                    tot_alicu_general_base_v -= l.price_subtotal  # Base imponible
                                    tot_alicu_general_debi_v -= alicuota_v  # DÉBITO FISCAL
                                    tot_alicu_general_reten_v -= reten_iva_v

                                # Ventas Internas Gravadas por Alicuota General más Adicional item 4
                                # Iva general + adicional 16% + 15%
                                elif l.invoice_line_tax_ids.amount == 31:
                                    tot_alicu_gener_adic_base_v -= l.price_subtotal  # Base imponible
                                    tot_alicu_gener_adic_debi_v -= alicuota_v  # DÉBITO FISCAL
                                    tot_alicu_gener_adic_reten_v -= reten_iva_v

                                # Ventas Internas Gravadas por Alicuota Reducida item 5
                                # Iva Reducido
                                elif l.invoice_line_tax_ids.amount == 8:
                                    tot_alicu_reducida_base_v -= l.price_subtotal  # Base imponible
                                    tot_alicu_reducida_debi_v -= alicuota_v  # DÉBITO FISCAL
                                    tot_alicu_reducida_reten_v -= reten_iva_v

        # Total Ventas y Debitos Fiscales para Efectos de Determinación item 6   # item para el item 9
        tot_vet_efect_determ_base = (tot_execto_mes_v + tot_alicu_general_base_v + tot_alicu_gener_adic_base_v + tot_alicu_reducida_base_v)
        tot_vet_efect_determ_debi = (tot_alicu_general_debi_v + tot_alicu_gener_adic_debi_v + tot_alicu_reducida_debi_v)                  # item para el item 9

       # Calculos de Compras item 10 - 26 CRÉDITOS FISCALES

        tot_execto_mes_c = 0

        tot_alicu_general_base_c = 0
        tot_alicu_general_debi_c = 0
        tot_alicu_general_reten_c = 0

        tot_alicu_reducida_base_c = 0
        tot_alicu_reducida_debi_c = 0
        tot_alicu_reducida_reten_c = 0

        tot_alicu_gener_adic_base_c = 0
        tot_alicu_gener_adic_debi_c = 0
        tot_alicu_gener_adic_reten_c = 0

        for invoice_c in self.invoices_c:

            ids_accounts = invoice_c.id

            for l in invoice_c.invoice_line_ids:
                if l.invoice_id.id == ids_accounts:
                    if l.invoice_line_tax_ids.amount == 0:

                        # Compras no Gravadas y/o sin Derecho a Credito Fiscal item 10
                        tot_execto_mes_c += l.price_subtotal

                    elif l.invoice_line_tax_ids.amount != 0:

                        # Calculos de Alicuaotas
                        alicuota_c = ((l.price_subtotal * l.invoice_line_tax_ids.amount) / 100)

                        # Calculos de Retencion
                        reten_iva_c = ((alicuota_c * invoice_c.retiva_id.porc_ret) / 100)

                        # Compras Gravadas por Alicuota General item 14
                        # Iva General
                        if l.invoice_line_tax_ids.amount == 16:
                            tot_alicu_general_base_c += l.price_subtotal  # Base imponible
                            tot_alicu_general_debi_c += alicuota_c  # DÉBITO FISCAL
                            # tot_alicu_general_reten_c += reten_iva_c

                        # Compras Gravadas por Alicuota General más Alicuota Adicional item 15
                        # Iva general + adicional 16% + 15%
                        elif l.invoice_line_tax_ids.amount == 31:
                            tot_alicu_gener_adic_base_c += l.price_subtotal  # Base imponible
                            tot_alicu_gener_adic_debi_c += alicuota_c  # DÉBITO FISCAL
                            # tot_alicu_gener_adic_reten_c += reten_iva_c

                        # Compras Gravadas por Alicuota Reducida item 16
                        # Iva Reducido
                        elif l.invoice_line_tax_ids.amount == 8:
                            tot_alicu_reducida_base_c += l.price_subtotal  # Base imponible
                            tot_alicu_reducida_debi_c += alicuota_c  # DÉBITO FISCAL
                            # tot_alicu_reducida_reten_c += reten_iva_c

                else:

                    if l.invoice_line_tax_ids.amount == 0:

                        tot_execto_mes_c -= l.price_subtotal

                    elif l.invoice_line_tax_ids.amount != 0:

                        # Calculos de Alicuaotas
                        alicuota_c = ((l.price_subtotal * l.invoice_line_tax_ids.amount) / 100)

                        # Calculos de Retencion
                        reten_iva_c = ((alicuota_c * invoice_c.retiva_id.porc_ret) / 100)

                        # Compras Gravadas por Alicuota General item 14
                        # Iva General
                        if l.invoice_line_tax_ids.amount == 16:
                            tot_alicu_general_base_c -= l.price_subtotal  # Base imponible
                            tot_alicu_general_debi_c -= alicuota_c  # DÉBITO FISCAL
                            # tot_alicu_general_reten_c -= reten_iva_c

                        # Compras Gravadas por Alicuota General más Alicuota Adicional item 15
                        # Iva general + adicional 16% + 15%
                        elif l.invoice_line_tax_ids.amount == 31:
                            tot_alicu_gener_adic_base_c -= l.price_subtotal  # Base imponible
                            tot_alicu_gener_adic_debi_c -= alicuota_c  # DÉBITO FISCAL
                            # tot_alicu_gener_adic_reten_c -= reten_iva_c

                        # Compras Gravadas por Alicuota Reducida item 16
                        # Iva Reducido
                        elif l.invoice_line_tax_ids.amount == 8:
                            tot_alicu_reducida_base_c -= l.price_subtotal  # Base imponible
                            tot_alicu_reducida_debi_c -= alicuota_c  # DÉBITO FISCAL
                            # tot_alicu_reducida_reten_c -= reten_iva_c


        # Total Compras y Créditos Fiscales del Período item 17
        tot_comp_fisc_periodo_base = (tot_execto_mes_c + tot_alicu_general_base_c + tot_alicu_gener_adic_base_c + tot_alicu_reducida_base_c)
        tot_comp_fisc_periodo_debi = (tot_alicu_general_debi_c + tot_alicu_gener_adic_debi_c + tot_alicu_reducida_debi_c) #Creditos Fiscales Totalmente Deducibles item 18

        # Créditos Fiscales Producto de la Aplicación del Porcentaje de la Prorrata

        # Total Créditos Fiscales Deducibles item item 20 -> (item 18 - item 19)falta
        tot_credi_fisc_deduc = tot_comp_fisc_periodo_debi

        # Exedente Créditos Fiscales del Mes Anterior item 21 -> Total del item 28 del mes anterior
        tot_exed_credi_fisc_m_ant = 0 #falta

        # Total Creditos Fiscales item 26 -> Total del item 20 al 25
        tot_credi_fiscales = (tot_credi_fisc_deduc + tot_exed_credi_fisc_m_ant)#faltan


        ## AUTOLIQUIDACIÓN

        # Total Cuota Tributaria del Período item 27 -> resta el item 9 con el item 26, solo si 9 es mayor que el item 26
        if tot_vet_efect_determ_debi > tot_credi_fiscales:

            tot_cuot_trib_periodo = tot_vet_efect_determ_debi - tot_credi_fiscales

        # Exedente de Crédito Fiscal para el mes Siguiente item 28 -> resta del item 26 con el item 9, solo si 26 es mayor que el item 26
        if tot_credi_fiscales > tot_vet_efect_determ_debi:
            exedente_credi_fiscal = tot_credi_fiscales - tot_vet_efect_determ_debi

        # Sub- Total Impuesto a Pagar item 32 -> total de item 27
        sub_tot_autoliq = tot_cuot_trib_periodo

        ## RETENCIONES IVA

        # Retenciones IVA Acumuladas por Descontar item 33 ->  total de item 39 del mes anterior
        reten_iva_acumul_desc = 0

        # Retenciones del IVA del Periodo item 34 -> retencion de iva de las ventas del mes
        tot_reten_iva_periodo =  (tot_alicu_general_reten_v + tot_alicu_gener_adic_reten_v + tot_alicu_reducida_reten_v)

        # Total Retenciones del IVA item 37 -> suma de iten 33 al 36
        tot_reten_iva = (reten_iva_acumul_desc + tot_reten_iva_periodo)

        # Retenciones del IVA Soportadas y Descontadas item 38 -> colocar monto de items 32 si dicho monto es menor a items 37. Si items 32 es mayor a items 37 colocar monto de items 37
        if tot_reten_iva > sub_tot_autoliq: # item37 - 32
            reten_iva_soport_descont = sub_tot_autoliq

        elif sub_tot_autoliq > tot_reten_iva: # item 32 - 37
            reten_iva_soport_descont = tot_reten_iva

        # Saldo Retenciones del IVA no Aplicado item 39 -> item 37 - 38
        sald_reten_iva_no_apli = (tot_reten_iva - reten_iva_soport_descont)

        #Sub- Total Impuesto a Pagar item 40 -> resta de item 32 con 38
        sub_tot_reten_iva = (sub_tot_autoliq - reten_iva_soport_descont)

        ##PERCEPCIÓN

        # Saldo de Percepciones en Aduanas no Aplicado item 47
        sal_percep_adu_no_apli = 0

        # Total a Pagar item 48 -> resta del item 40 con 47
        total_a_pag_percepcion = sub_tot_reten_iva - sal_percep_adu_no_apli




    def get_invoice(self):
        self.facturas_ids = self.env['account.invoice'].search([('date_invoice','<=', self.date_to),('date_invoice','>=',self.date_from),('type','!=','in_invoice'),('type','!=','in_refund'),('state','!=','draft')])
        _logger.info("\n\n\n {} \n\n\n".format(self.facturas_ids))

        self.retiva_ids = self.env['snc.retiva.partners.lines'].search([('monto_sujeto', '!=', 0)])
        _logger.info("\n\n\n {} \n\n\n".format(self.retiva_ids))


    @api.multi
    def print_facturas(self):
        self.get_invoice()
        return self.env["report"].get_action(self, 'resumen_declaracion_iva.libro_resumen_iva')

    
    @api.multi
    def cont_row(self):
        row = 0
        for record in self.facturas_ids:
            row +=1
        return row


    @api.multi
    def generate_xls_report(self):

        self.ensure_one()

        wb1 = xlwt.Workbook(encoding='utf-8')
        ws1 = wb1.add_sheet('Invoices Details')
        fp = cStringIO.StringIO()



        #Content/Text style

        lat_izqu = xlwt.easyxf("font: name Helvetica size 10 px, bold 1, height 170;  borders: left thin, top thin;")
        lat_izqu_1 = xlwt.easyxf("font: name Helvetica size 10 px, bold 1, height 170;  borders: top thin;")
        part_sup = xlwt.easyxf("font: name Helvetica size 10 px, bold 1, height 170; borders: right thin, top thin;")
        lat_dech = xlwt.easyxf("font: name Helvetica size 10 px, bold 1, height 170; borders: right thin, top thin;")
        #part_inf
        #derec
        #izqu

        header_content_style = xlwt.easyxf("font: name Helvetica size 50 px, bold 1, height 170;")
        sub_header_style = xlwt.easyxf("font: name Helvetica size 10 px, bold 1, height 170; borders: left thin, right thin, top thin, bottom thin;")
        sub_header_content_style = xlwt.easyxf("font: name Helvetica size 10 px, height 170;")
        line_content_style = xlwt.easyxf("font: name Helvetica, height 170;")
        width_1 = 600
        row = 1
        col = 0
        ws1.row(row).height = 200
        ws1.col(col+1).width = 600
        ws1.col(col+2).width = 900





        ws1.write(row,col+1,"", lat_izqu)
        ws1.write(row,col+2,"", lat_izqu_1)
        ws1.write(row,col+3,"", lat_izqu_1)
        ws1.write(row,col+4, "", lat_izqu)
        ws1.write(row,col+5, "", lat_izqu_1)
        ws1.write(row,col+6, "", lat_izqu_1)
        row += 1

        ws1.write(row, col + 1, "",line_content_style)
        ws1.write(row, col + 2, "Razon social: My Company",line_content_style)
        ws1.write(row, col + 3, "",line_content_style)
        ws1.write(row, col + 4, "",line_content_style)
        ws1.write(row, col + 5, "",line_content_style)
        ws1.write(row, col + 6, "",line_content_style)
        row += 1

        ws1.write(row, col + 1, "", line_content_style)
        ws1.write(row, col + 2, "R.I.F.: j-12345678-0", line_content_style)
        ws1.write(row, col + 3, "", line_content_style)
        ws1.write(row, col + 4, "", line_content_style)
        ws1.write(row, col + 5, "", line_content_style)
        ws1.write(row, col + 6, "", line_content_style)
        row += 1

        ws1.write(row, col + 1, "", line_content_style)
        ws1.write(row, col + 6, "", line_content_style)
        row += 1

        ws1.write(row, col + 1, "", line_content_style)
        ws1.write(row, col + 6, "", line_content_style)
        row += 1

        ws1.write(row, col + 1, "", line_content_style)
        ws1.write_merge(row,row,2,5, "Resumen de IVA Periodo: Agosto 2017", sub_header_style)
        ws1.write(row, col + 6, "", line_content_style)
        row += 1

        ws1.write(row, col + 1, "", line_content_style)
        ws1.write(row, col + 6, "", line_content_style)
        row += 1

        ws1.write(row, col + 1, "", line_content_style)
        ws1.write_merge(row, row, 2, 3, "DEBITOS FISCALES", sub_header_style)
        ws1.write(row, col + 4, "BASE IMPONIBLE", sub_header_style)
        ws1.write(row, col + 5, "DEBITO FISCAL", sub_header_style)
        ws1.write(row, col + 6, "", line_content_style)
        row += 1

        ws1.write(row, col + 1, "", line_content_style)
        ws1.write(row, col + 2, "1", sub_header_style)
        ws1.write(row, col + 3, "Ventas Internas no Gravadas", sub_header_style)
        ws1.write(row, col + 4, "0,00", sub_header_style)
        ws1.write(row, col + 5, "", line_content_style)
        ws1.write(row, col + 6, "", line_content_style)
        row += 1

        ws1.write(row, col + 1, "", line_content_style)
        ws1.write(row, col + 2, "2", sub_header_style)
        ws1.write(row, col + 3, "Ventas de Exportacion", sub_header_style)
        ws1.write(row, col + 4, "", sub_header_style)
        ws1.write(row, col + 5, "", line_content_style)
        ws1.write(row, col + 6, "", line_content_style)
        row += 1

        ws1.write(row, col + 1, "", line_content_style)
        ws1.write(row, col + 2, "3", sub_header_style)
        ws1.write(row, col + 3, "Ventas Internas Gravadas por Alicuota General", sub_header_style)
        ws1.write(row, col + 4, "12.434.426,14", sub_header_style)
        ws1.write(row, col + 5, "1.492.131,14", sub_header_style)
        ws1.write(row, col + 6, "", line_content_style)
        row += 1

        ws1.write(row, col + 1, "", line_content_style)
        ws1.write(row, col + 2, "4", sub_header_style)
        ws1.write(row, col + 3, "Ventas Internas Gravadas por Alicuota General mas Adicional", sub_header_style)
        ws1.write(row, col + 4, "", sub_header_style)
        ws1.write(row, col + 5, "", line_content_style)
        ws1.write(row, col + 6, "", line_content_style)
        row += 1

        ws1.write(row, col + 1, "", line_content_style)
        ws1.write(row, col + 2, "5", sub_header_style)
        ws1.write(row, col + 3, "Ventas Internas Gravadas por Alicuota Reducida", sub_header_style)
        ws1.write(row, col + 4, "", sub_header_style)
        ws1.write(row, col + 5, "0,00", sub_header_style)
        ws1.write(row, col + 6, "", line_content_style)
        row += 1

        ws1.write(row, col + 1, "", line_content_style)
        ws1.write(row, col + 2, "6", sub_header_style)
        ws1.write(row, col + 3, "Total Ventas y Debitos Fiscales para Efectos de Determinacion", sub_header_style)
        ws1.write(row, col + 4, "12.434.426,14", sub_header_style)
        ws1.write(row, col + 5, "1.492.131,14", line_content_style)
        ws1.write(row, col + 6, "", line_content_style)
        row += 1

        ws1.write(row, col + 1, "", line_content_style)
        ws1.write(row, col + 2, "7", sub_header_style)
        ws1.write(row, col + 3, "Ajustes a los Debitos Fiscales de Periodos Anteriores", sub_header_style)
        ws1.write(row, col + 4, "", sub_header_style)
        ws1.write(row, col + 5, "0,00", sub_header_style)
        ws1.write(row, col + 6, "", line_content_style)
        row += 1

        ws1.write(row, col + 1, "", line_content_style)
        ws1.write(row, col + 2, "8", sub_header_style)
        ws1.write(row, col + 3, "Certificados de Debitos Fiscales Exonerados", sub_header_style)
        ws1.write(row, col + 4, "", sub_header_style)
        ws1.write(row, col + 5, "0,00", sub_header_style)
        ws1.write(row, col + 6, "", line_content_style)
        row += 1

        ws1.write(row, col + 1, "", line_content_style)
        ws1.write(row, col + 2, "9", sub_header_style)
        ws1.write(row, col + 3, "Total Debitos Fiscales", sub_header_style)
        ws1.write(row, col + 4, "", sub_header_style)
        ws1.write(row, col + 5, "1.492.131,14", sub_header_style)
        ws1.write(row, col + 6, "", line_content_style)
        row += 1

        ws1.write(row, col + 1, "", line_content_style)
        ws1.write_merge(row, row, 2, 3, "CRÉDITOS FISCALES", sub_header_style)
        ws1.write(row, col + 4, "BASE IMPONIBLE", sub_header_style)
        ws1.write(row, col + 5, "DÉBITO FISCAL", sub_header_style)
        ws1.write(row, col + 6, "", line_content_style)
        row += 1

        ws1.write(row, col + 1, "", line_content_style)
        ws1.write(row, col + 2, "10", sub_header_style)
        ws1.write(row, col + 3, "Compras no Gravadas y/o sin Derecho a Credito Fiscal", sub_header_style)
        ws1.write(row, col + 4, "871.450,57", sub_header_style)
        ws1.write(row, col + 5, "", sub_header_style)
        ws1.write(row, col + 6, "", line_content_style)
        row += 1

        ws1.write(row, col + 1, "", line_content_style)
        ws1.write(row, col + 2, "11", sub_header_style)
        ws1.write(row, col + 3, "Importaciones Gravadas por Alicuota General", sub_header_style)
        ws1.write(row, col + 4, "0,00", sub_header_style)
        ws1.write(row, col + 5, "0,00", sub_header_style)
        ws1.write(row, col + 6, "", line_content_style)
        row += 1

        ws1.write(row, col + 1, "", line_content_style)
        ws1.write(row, col + 2, "12", sub_header_style)
        ws1.write(row, col + 3, "Importaciones Gravadas por Alicuota General más Alicuota Adicional", sub_header_style)
        ws1.write(row, col + 4, "", sub_header_style)
        ws1.write(row, col + 5, "", sub_header_style)
        ws1.write(row, col + 6, "", line_content_style)
        row += 1

        ws1.write(row, col + 1, "", line_content_style)
        ws1.write(row, col + 2, "13", sub_header_style)
        ws1.write(row, col + 3, "Importaciones Gravadas por Alicuota Reducida", sub_header_style)
        ws1.write(row, col + 4, "0,00", sub_header_style)
        ws1.write(row, col + 5, "0,00", sub_header_style)
        ws1.write(row, col + 6, "", line_content_style)
        row += 1

        ws1.write(row, col + 1, "", line_content_style)
        ws1.write(row, col + 2, "14", sub_header_style)
        ws1.write(row, col + 3, "Compras Gravadas por Alicuota General", sub_header_style)
        ws1.write(row, col + 4, "12.699.303,35", sub_header_style)
        ws1.write(row, col + 5, "1.523.916,40", sub_header_style)
        ws1.write(row, col + 6, "", line_content_style)
        row += 1

        ws1.write(row, col + 1, "", line_content_style)
        ws1.write(row, col + 2, "15", sub_header_style)
        ws1.write(row, col + 3, "Compras Gravadas por Alicuota General más Alicuota Adicional", sub_header_style)
        ws1.write(row, col + 4, "0,00", sub_header_style)
        ws1.write(row, col + 5, "0,00", sub_header_style)
        ws1.write(row, col + 6, "", line_content_style)
        row += 1

        ws1.write(row, col + 1, "", line_content_style)
        ws1.write(row, col + 2, "16", sub_header_style)
        ws1.write(row, col + 3, "Compras Gravadas por Alicuota Reducida", sub_header_style)
        ws1.write(row, col + 4, "1.630.000,00", sub_header_style)
        ws1.write(row, col + 5, "130.400,00", sub_header_style)
        ws1.write(row, col + 6, "", line_content_style)
        row += 1

        ws1.write(row, col + 1, "", line_content_style)
        ws1.write(row, col + 2, "17", sub_header_style)
        ws1.write(row, col + 3, "Total Compras y Créditos Fiscales del Período", sub_header_style)
        ws1.write(row, col + 4, "15.200.753,92", sub_header_style)
        ws1.write(row, col + 5, "1.654.316,40", sub_header_style)
        ws1.write(row, col + 6, "", line_content_style)
        row += 1

        ws1.write(row, col + 1, "", line_content_style)
        ws1.write(row, col + 2, "18", sub_header_style)
        ws1.write(row, col + 3, "Creditos Fiscales Totalmente Deducibles ", sub_header_style)
        ws1.write(row, col + 4, "", sub_header_style)
        ws1.write(row, col + 5, "1.654.316,40", sub_header_style)
        ws1.write(row, col + 6, "", line_content_style)
        row += 1

        ws1.write(row, col + 1, "", line_content_style)
        ws1.write(row, col + 2, "19", sub_header_style)
        ws1.write(row, col + 3, "Créditos Fiscales Producto de la Aplicación del Porcentaje de la Prorrata", sub_header_style)
        ws1.write(row, col + 4, "", sub_header_style)
        ws1.write(row, col + 5, "", sub_header_style)
        ws1.write(row, col + 6, "", line_content_style)
        row += 1

        ws1.write(row, col + 1, "", line_content_style)
        ws1.write(row, col + 2, "20", sub_header_style)
        ws1.write(row, col + 3, "Total Créditos Fiscales Deducibles", sub_header_style)
        ws1.write(row, col + 4, "", sub_header_style)
        ws1.write(row, col + 5, "0,00", sub_header_style)
        ws1.write(row, col + 6, "", line_content_style)
        row += 1

        ws1.write(row, col + 1, "", line_content_style)
        ws1.write(row, col + 2, "21", sub_header_style)
        ws1.write(row, col + 3, "Exedente Créditos Fiscales del Mes Anterior ", sub_header_style)
        ws1.write(row, col + 4, "", sub_header_style)
        ws1.write(row, col + 5, "0,00", sub_header_style)
        ws1.write(row, col + 6, "", line_content_style)
        row += 1

        ws1.write(row, col + 1, "", line_content_style)
        ws1.write(row, col + 2, "22", sub_header_style)
        ws1.write(row, col + 3, "Reintegro Solicitado (sólo exportadores)", sub_header_style)
        ws1.write(row, col + 4, "", sub_header_style)
        ws1.write(row, col + 5, "", sub_header_style)
        ws1.write(row, col + 6, "", line_content_style)
        row += 1

        ws1.write(row, col + 1, "", line_content_style)
        ws1.write(row, col + 2, "23", sub_header_style)
        ws1.write(row, col + 3, "Reintegro (sólo quien suministre bienes o presten servicios a entes exonerados)", sub_header_style)
        ws1.write(row, col + 4, "", sub_header_style)
        ws1.write(row, col + 5, "", sub_header_style)
        ws1.write(row, col + 6, "", line_content_style)
        row += 1

        ws1.write(row, col + 1, "", line_content_style)
        ws1.write(row, col + 2, "24", sub_header_style)
        ws1.write(row, col + 3, "Ajustes a los Créditos Fiscales de Periodos Anteriores.", sub_header_style)
        ws1.write(row, col + 4, "0,00", sub_header_style)
        ws1.write(row, col + 5, "", sub_header_style)
        ws1.write(row, col + 6, "", line_content_style)
        row += 1

        ws1.write(row, col + 1, "", line_content_style)
        ws1.write(row, col + 2, "25", sub_header_style)
        ws1.write(row, col + 3, "Certificados de Débitos Fiscales Exonerados (emitidos de entes exonerados) Registrados en el periodo", sub_header_style)
        ws1.write(row, col + 4, "", sub_header_style)
        ws1.write(row, col + 5, "", sub_header_style)
        ws1.write(row, col + 6, "", line_content_style)
        row += 1

        ws1.write(row, col + 1, "", line_content_style)
        ws1.write(row, col + 2, "26", sub_header_style)
        ws1.write(row, col + 3, "Total Creditos Fiscales ", sub_header_style)
        ws1.write(row, col + 4, "", sub_header_style)
        ws1.write(row, col + 5, "1.654.316,40", sub_header_style)
        ws1.write(row, col + 6, "", line_content_style)
        row += 1

        ws1.write(row, col + 1, "", line_content_style)
        ws1.write_merge(row, row, 2, 5, "AUTOLIQUIDACIÓN", sub_header_style)
        ws1.write(row, col + 6, "", line_content_style)
        row += 1

        ws1.write(row, col + 1, "", line_content_style)
        ws1.write(row, col + 2, "27", sub_header_style)
        ws1.write(row, col + 3, "Total Cuota Tributaria del Período. ", sub_header_style)
        ws1.write(row, col + 4, "", sub_header_style)
        ws1.write(row, col + 5, "1.654.316,40", sub_header_style)
        ws1.write(row, col + 6, "", line_content_style)
        row += 1

        ws1.write(row, col + 1, "", line_content_style)
        ws1.write(row, col + 2, "28", sub_header_style)
        ws1.write(row, col + 3, "Exedente de Crédito Fiscal para el mes Siguiente", sub_header_style)
        ws1.write(row, col + 4, "", sub_header_style)
        ws1.write(row, col + 5, "162.185,27", sub_header_style)
        ws1.write(row, col + 6, "", line_content_style)
        row += 1

        ws1.write(row, col + 1, "", line_content_style)
        ws1.write(row, col + 2, "29", sub_header_style)
        ws1.write(row, col + 3, "Impuesto Pagado en Declaración(es) Sustituida(s)", sub_header_style)
        ws1.write(row, col + 4, "", sub_header_style)
        ws1.write(row, col + 5, "", sub_header_style)
        ws1.write(row, col + 6, "", line_content_style)
        row += 1

        ws1.write(row, col + 1, "", line_content_style)
        ws1.write(row, col + 2, "30", sub_header_style)
        ws1.write(row, col + 3, "Retenciones Descontadas en Declaración(es) Sustitutiva(s)", sub_header_style)
        ws1.write(row, col + 4, "", sub_header_style)
        ws1.write(row, col + 5, "", sub_header_style)
        ws1.write(row, col + 6, "", line_content_style)
        row += 1

        ws1.write(row, col + 1, "", line_content_style)
        ws1.write(row, col + 2, "31", sub_header_style)
        ws1.write(row, col + 3, "Percepciones Descontadas en Declaración(es) Sustitutiva(s)", sub_header_style)
        ws1.write(row, col + 4, "", sub_header_style)
        ws1.write(row, col + 5, "", sub_header_style)
        ws1.write(row, col + 6, "", line_content_style)
        row += 1

        ws1.write(row, col + 1, "", line_content_style)
        ws1.write(row, col + 2, "32", sub_header_style)
        ws1.write(row, col + 3, "Sub- Total Impuesto a Pagar", sub_header_style)
        ws1.write(row, col + 4, "", sub_header_style)
        ws1.write(row, col + 5, "0,00", sub_header_style)
        ws1.write(row, col + 6, "", line_content_style)
        row += 1

        ws1.write(row, col + 1, "", line_content_style)
        ws1.write_merge(row, row, 2, 5, "RETENCIONES IVA", sub_header_style)
        ws1.write(row, col + 6, "", line_content_style)
        row += 1

        ws1.write(row, col + 1, "", line_content_style)
        ws1.write(row, col + 2, "33", sub_header_style)
        ws1.write(row, col + 3, "Retenciones IVA Acumuladas por Descontar", sub_header_style)
        ws1.write(row, col + 4, "0,00", sub_header_style)
        ws1.write(row, col + 5, "", sub_header_style)
        ws1.write(row, col + 6, "", line_content_style)
        row += 1

        ws1.write(row, col + 1, "", line_content_style)
        ws1.write(row, col + 2, "34", sub_header_style)
        ws1.write(row, col + 3, "Retenciones del IVA del Periodo", sub_header_style)
        ws1.write(row, col + 4, "1.397.292,04", sub_header_style)
        ws1.write(row, col + 5, "", sub_header_style)
        ws1.write(row, col + 6, "", line_content_style)
        row += 1

        ws1.write(row, col + 1, "", line_content_style)
        ws1.write(row, col + 2, "35", sub_header_style)
        ws1.write(row, col + 3, "Créditos del IVA Adquiridos por Cesiones de Retenciones", sub_header_style)
        ws1.write(row, col + 4, "", sub_header_style)
        ws1.write(row, col + 5, "", sub_header_style)
        ws1.write(row, col + 6, "", line_content_style)
        row += 1

        ws1.write(row, col + 1, "", line_content_style)
        ws1.write(row, col + 2, "36", sub_header_style)
        ws1.write(row, col + 3, "Recuperaciones del IVA Retenciones Solicitadas", sub_header_style)
        ws1.write(row, col + 4, "", sub_header_style)
        ws1.write(row, col + 5, "", sub_header_style)
        ws1.write(row, col + 6, "", line_content_style)
        row += 1

        ws1.write(row, col + 1, "", line_content_style)
        ws1.write(row, col + 2, "37", sub_header_style)
        ws1.write(row, col + 3, "Total Retenciones del IVA", sub_header_style)
        ws1.write(row, col + 4, "1.397.292,04", sub_header_style)
        ws1.write(row, col + 5, "", sub_header_style)
        ws1.write(row, col + 6, "", line_content_style)
        row += 1

        ws1.write(row, col + 1, "", line_content_style)
        ws1.write(row, col + 2, "38", sub_header_style)
        ws1.write(row, col + 3, "Retenciones del IVA Soportadas y Descontadas", sub_header_style)
        ws1.write(row, col + 4, "", sub_header_style)
        ws1.write(row, col + 5, "0,00", sub_header_style)
        ws1.write(row, col + 6, "", line_content_style)
        row += 1

        ws1.write(row, col + 1, "", line_content_style)
        ws1.write(row, col + 2, "39", sub_header_style)
        ws1.write(row, col + 3, "Saldo Retenciones del IVA no Aplicado ", sub_header_style)
        ws1.write(row, col + 4, "1.397.292,04", sub_header_style)
        ws1.write(row, col + 5, "", sub_header_style)
        ws1.write(row, col + 6, "", line_content_style)
        row += 1

        ws1.write(row, col + 1, "", line_content_style)
        ws1.write(row, col + 2, "40", sub_header_style)
        ws1.write(row, col + 3, "Sub- Total Impuesto a Pagar", sub_header_style)
        ws1.write(row, col + 4, "", sub_header_style)
        ws1.write(row, col + 5, "0,00", sub_header_style)
        ws1.write(row, col + 6, "", line_content_style)
        row += 1

        ws1.write(row, col + 1, "", line_content_style)
        ws1.write_merge(row, row, 2, 5, "PERCEPCIÓN", sub_header_style)
        ws1.write(row, col + 6, "", line_content_style)
        row += 1

        ws1.write(row, col + 1, "", line_content_style)
        ws1.write(row, col + 2, "41", sub_header_style)
        ws1.write(row, col + 3, "Percepciones Acumuladas en Importaciones por Descontar", sub_header_style)
        ws1.write(row, col + 4, "", sub_header_style)
        ws1.write(row, col + 5, "", sub_header_style)
        ws1.write(row, col + 6, "", line_content_style)
        row += 1

        ws1.write(row, col + 1, "", line_content_style)
        ws1.write(row, col + 2, "42", sub_header_style)
        ws1.write(row, col + 3, "Percepciones del Periodo", sub_header_style)
        ws1.write(row, col + 4, "", sub_header_style)
        ws1.write(row, col + 5, "", sub_header_style)
        ws1.write(row, col + 6, "", line_content_style)
        row += 1

        ws1.write(row, col + 1, "", line_content_style)
        ws1.write(row, col + 2, "43", sub_header_style)
        ws1.write(row, col + 3, "Creédtos Adquiridos por Cesiones de Percepciones", sub_header_style)
        ws1.write(row, col + 4, "", sub_header_style)
        ws1.write(row, col + 5, "", sub_header_style)
        ws1.write(row, col + 6, "", line_content_style)
        row += 1

        ws1.write(row, col + 1, "", line_content_style)
        ws1.write(row, col + 2, "44", sub_header_style)
        ws1.write(row, col + 3, "Recuperaciones Percepciones Solicitado", sub_header_style)
        ws1.write(row, col + 4, "", sub_header_style)
        ws1.write(row, col + 5, "", sub_header_style)
        ws1.write(row, col + 6, "", line_content_style)
        row += 1

        ws1.write(row, col + 1, "", line_content_style)
        ws1.write(row, col + 2, "45", sub_header_style)
        ws1.write(row, col + 3, "Total Percepciones", sub_header_style)
        ws1.write(row, col + 4, "0,00", sub_header_style)
        ws1.write(row, col + 5, "", sub_header_style)
        ws1.write(row, col + 6, "", line_content_style)
        row += 1

        ws1.write(row, col + 1, "", line_content_style)
        ws1.write(row, col + 2, "46", sub_header_style)
        ws1.write(row, col + 3, "Percepciones en Aduanas Descontadas", sub_header_style)
        ws1.write(row, col + 4, "", sub_header_style)
        ws1.write(row, col + 5, "", sub_header_style)
        ws1.write(row, col + 6, "", line_content_style)
        row += 1

        ws1.write(row, col + 1, "", line_content_style)
        ws1.write(row, col + 2, "47", sub_header_style)
        ws1.write(row, col + 3, "Saldo de Percepciones en Aduanas no Aplicado", sub_header_style)
        ws1.write(row, col + 4, "", sub_header_style)
        ws1.write(row, col + 5, "0,00", sub_header_style)
        ws1.write(row, col + 6, "", line_content_style)
        row += 1

        ws1.write(row, col + 1, "", line_content_style)
        ws1.write(row, col + 2, "48", sub_header_style)
        ws1.write(row, col + 3, "Total a Pagar ", sub_header_style)
        ws1.write(row, col + 4, "", sub_header_style)
        ws1.write(row, col + 5, "0,00", sub_header_style)
        ws1.write(row, col + 6, "", line_content_style)
        row += 1

        '''
        row += 2
        ws1.write_merge(row, row, 1, 2, "Razon Social :", sub_header_style)
        ws1.write_merge(row, row, 3, 5, 'a' , sub_header_content_style)

        
        row+=1
        ws1.write_merge(row, row, 1, 2, "RIF:", sub_header_style)
        ws1.write_merge(row, row, 3, 5,  str(self.company_id.rif), sub_header_content_style)
        row+=1
        ws1.write_merge(row, row, 1, 2, "Direccion Fiscal:", sub_header_style)
        ws1.write_merge(row, row, 3, 6, str(self.company_id.street), sub_header_content_style)
        row +=1
        ws1.write(row, col+1, "Desde :", sub_header_style)
        ws1.write(row, col+2, datetime.strftime(datetime.strptime(self.date_from,DEFAULT_SERVER_DATE_FORMAT),"%d/%m/%Y"), sub_header_content_style)
        row += 1
        ws1.write(row, col+1, "Hasta :", sub_header_style)
        ws1.write(row, col+2, datetime.strftime(datetime.strptime(self.date_to,DEFAULT_SERVER_DATE_FORMAT),"%d/%m/%Y"), sub_header_content_style)
        row += 2
        ws1.write_merge(row, row, 14, 16,"Ventas Internas o Exportacion Gravadas",sub_header_style)
        row += 1
        ws1.write(row,col+1,"#",sub_header_style)
        ws1.write(row,col+2,"Fecha Documento",sub_header_style)
        ws1.write(row,col+3,"RIF",sub_header_style)
        ws1.write(row,col+4,"Nombre Razon Social",sub_header_style)
        ws1.write(row,col+5,"Numero de Planilla de exportacion",sub_header_style)
        ws1.write(row,col+6,"Nro Factura / Entrega",sub_header_style)
        ws1.write(row,col+7,"Nro de Control",sub_header_style)
        ws1.write(row,col+8,"Nro Factura Afectada",sub_header_style)
        ws1.write(row,col+9,"Nro de nota de debito",sub_header_style)
        ws1.write(row,col+10,"Numero de nota de credito",sub_header_style)
        ws1.write(row,col+11,"Ventas Incluyendo el IVA",sub_header_style)
        ws1.write(row,col+12,"Ventas internas o exoneraciones no gravadas",sub_header_style)
        ws1.write(row,col+13,"Ventas internas o exportaciones exoneradas",sub_header_style)
        ws1.write(row,col+14,"Base Imponible",sub_header_style)
        ws1.write(row,col+15,"'%'Alicuota",sub_header_style)
        ws1.write(row,col+16,"Impuesto IVA",sub_header_style) 
        ws1.write(row,col+17,"IVA Retenido Comprador",sub_header_style)
        ws1.write(row,col+18,"Nro. Comprobante de Retencion",sub_header_style)
        ws1.write(row,col+19,"Fecha comp",sub_header_style)


        row += 1
        


        all_inv_total = 0
        num2 = 0
        total_internas = 0
        total_iva = 0
        total_imponible = 0
        total_con_IVA = 0
        reten_iva_total = 0
        alicuota = 0
        alicuota_reten = 0
        alicuota_porcent = ''
        alicuota_porcent_reten = ''
        tax_general = 0
        tax_reducido = 0
        base_general = 0
        base_reducido = 0
        cont_execto = 0
        t_retenido=0


        for invoice in self.invoices:


            num2 += 1
            num = str(invoice)

            # --------------------------------------
            ws1.write(row,col+1,num2,line_content_style) #contador
            ws1.write(row,col+2,invoice.date_invoice,line_content_style) # fecha de documento
            ws1.write(row,col+3,invoice.rif,line_content_style)#rif
            ws1.write(row,col+4,invoice.partner_id.name,line_content_style)# Nombre Razon social
            ws1.write(row,col+5,"",line_content_style)# numero de planilla de exportacion


            if (invoice.origin == 0):
                ws1.write(row,col+6,invoice.move_id.name,line_content_style)#nro de factura
            else:
                ws1.write(row, col + 6,"", line_content_style)


            ws1.write(row,col+7,invoice.invoice_sequence,line_content_style)#nro de control


            if (invoice.origin == 0):
                ws1.write(row,col+8,"",line_content_style)#nro factura afectada
            else:
                ws1.write(row,col+8,invoice.origin,line_content_style)


            ws1.write(row,col+9,invoice.supplier_control_number,line_content_style)#nro de nota de debito


            if (invoice.origin == 0):
                ws1.write(row,col+10,"",line_content_style)#nro de nota de credito
            else:
                ws1.write(row,col+10,invoice.move_id.name,line_content_style)

            ## traer datos del iva para saber las exentas y exoneradas

            ids_accounts = invoice.id
            cont_execto = 0

            for l in invoice.invoice_line_ids:
                if l.invoice_id.id == ids_accounts:
                    if l.invoice_line_tax_ids.amount == 0:
                        cont_execto += l.price_subtotal

            # =========================================================


            if (invoice.amount_tax == 0):  #ventas incluyendo iva
                if invoice.origin == 0:
                    ws1.write(row,col+11,cont_execto,line_content_style)
                else:
                    ws1.write(row,col+11,0,line_content_style)
                ws1.write(row,col+11,"",line_content_style)
            else:
                ws1.write(row,col+11,(invoice.amount_total), line_content_style)
                total_con_IVA += (invoice.amount_total)

            if (invoice.amount_tax != 0):   #ventas internas o exoneraciones no gravadas
                ws1.write(row,col+12,cont_execto,line_content_style)
                total_internas += cont_execto
            else:
                ws1.write(row,col+12,0,line_content_style)


            ws1.write(row,col+13,0,line_content_style)#ventas internas o exportaciones exoneradas


            if (invoice.amount_tax != 0):  #base imponible
                ws1.write(row,col+14,(invoice.amount_untaxed-cont_execto),line_content_style)  # Base Imponible
                total_imponible += invoice.amount_untaxed-cont_execto
            else:
                ws1.write(row,col+14,0,line_content_style)

            alicuota = (invoice.amount_tax*100/invoice.amount_untaxed)  # '%'Alic #%alicuota
            if alicuota >= 0.1 and alicuota <= 16.00:
                alicuota_porcent = 16
                tax_general += invoice.amount_tax
                base_general += invoice.amount_untaxed-cont_execto
            elif alicuota >= 6.00 and alicuota <= 9.00:
                alicuota_porcent = 8
                tax_reducido += invoice.amount_tax
                base_reducido += invoice.amount_untaxed-cont_execto
            elif alicuota >= 0 and alicuota <= 0.09:
                alicuota_porcent = 0
            ws1.write(row, col + 15, alicuota_porcent, line_content_style)


            if (invoice.amount_tax != 0):
                ws1.write(row,col+16,invoice.amount_tax,line_content_style)#impuesto IVA
                total_iva += invoice.amount_tax
            else:
                ws1.write(row,col+16,0,line_content_style)


            ## traer datos retencion de iva de otra tabla=============
            ids_accounts = invoice.id

            self.retivas = self.env['snc.retiva.partners.lines'].search ([('invoice_id','=',ids_accounts)])

            retenido = abs(self.retivas.importe_retenido)
            n_comprob = self.retivas.retiva_partner_id.name
            fec_comprob = self.retivas.retiva_partner_id.fecha_contabilizacion

            t_retenido = t_retenido+retenido
            # =========================================================

            ws1.write(row,col+17,retenido,line_content_style)#IVA RETENIDO COMPRADOR
            ws1.write(row,col+18,n_comprob,line_content_style)#nro. comprobante de retencion
            ws1.write(row,col+19,fec_comprob,line_content_style)#fecha de comp

            row +=1
            #all_inv_total += invoice.amount_total


        row +=1
        ws1.write(row,col+9,"TOTALES:",sub_header_style)
        ws1.write(row,col+11,total_con_IVA,sub_header_style) #Total con IVA
        ws1.write(row,col+12,total_internas,sub_header_style) # EXONERADAS NO AGRAVADAS
        ws1.write(row,col+13,0,sub_header_style) # exoneradas
        ws1.write(row,col+14,total_imponible,sub_header_style)# base imponible
        ws1.write(row,col+16,total_iva,sub_header_style) #iva
        ws1.write(row,col+17,t_retenido,sub_header_style) #retencion de iva
        #ws1.write(row,col+19,total_iva,sub_header_style)
        
        row +=2
        ws1.write(row,col+16,"Base Imponible",sub_header_style)
        ws1.write(row,col+17,"Debito Fiscal",sub_header_style)
        ws1.write_merge(row, row, 18, 19,"IVA retenido por comp.",sub_header_style)
        row+=1
        ws1.write_merge(row, row, 11, 15,"Ventas de Exportacion",sub_header_style)
        row+=1
        ws1.write_merge(row, row, 11, 15,"Ventas Internas Afectadas solo Alicuota General",sub_header_style)
        ws1.write(row, col + 16, base_general, line_content_style)
        ws1.write(row, col + 17, tax_general, line_content_style)
        ws1.write(row, col + 18, t_retenido, line_content_style)
        row+=1
        ws1.write_merge(row, row, 11, 15,"Ventas Internas Afectadas solo Alicuota General + Adicional",sub_header_style)
        row+=1
        ws1.write_merge(row, row, 11, 15,"Ventas Internas Afectadas solo Alicuota Reducida",sub_header_style)
        ws1.write(row, col + 16, base_reducido, line_content_style)
        ws1.write(row, col + 17, tax_reducido, line_content_style)
        ws1.write(row, col + 18, '', line_content_style)
        row+=1
        ws1.write_merge(row, row, 11, 15,"Ventas Internas Exoneradas",sub_header_style)
        row+=1
        ws1.write_merge(row, row, 11, 15,"Ventas Internas No Gravadas",sub_header_style)
        ws1.write(row, col + 16, total_internas, line_content_style)
        ws1.write(row, col + 17, '', line_content_style)
        ws1.write(row, col + 18, '', line_content_style)
        row+=1
        ws1.write_merge(row, row, 11, 15,"Total",sub_header_style)
        ws1.write(row, col + 16, (base_general + base_reducido + total_internas), line_content_style)
        ws1.write(row, col + 17, (tax_general + tax_reducido), line_content_style)
        ws1.write(row, col + 18,t_retenido, line_content_style)'''
        
        wb1.save(fp)
        out = base64.encodestring(fp.getvalue())
        self.write({'state': 'get', 'report': out, 'name':'invoices_detail.xls'})
        return {
            'type': 'ir.actions.act_window',
            'res_model': 'account.wizard.resumen.iva',
            'view_mode': 'form',
            'view_type': 'form',
            'res_id': self.id,
            'views': [(False, 'form')],
            'target': 'new',
        }
   
